<!DOCTYPE html>
<html lang="en">
  <head>
    <?php get_header(); ?>
  </head>
  <body>
        <section class="home-slider owl-carousel img" style="background-image: url(<?php echo get_theme_file_uri('images/bg_1.jpg') ?>);">
          <div class="slider-item" style="background-image: url(<?php echo get_theme_file_uri('images/bg_1.jpg') ?>);">
            <div class="overlay"></div>
              <div class="container">
                <div class="row slider-text justify-content-center align-items-center">
                  <div class="col-md-7 col-sm-12 text-center ftco-animate">
                    <h1 class="mb-3 mt-5 bread"><?php the_title() ?></h1>
                  <div class="breadcrumbs">
                    <?php
                        while(have_posts()){
                              the_post(); ?>
                            
                        <p class="breadcrumbs"><span class="nave-link"><a href="<?php echo get_post_type_archive_link('menu'); ?>">Menu </a></span> </p>                             
                    </div>

                  </div>
                </div>
              </div>
          </div>
        </section>


    <section class="ftco-section ftco-services">
        <div class="container">
            <h2 class="mb-4"><?php the_title() ?></h2>
              <div class="generic-content">
                  <a href="<?php the_post_thumbnail_url('menusLandscape'); ?>"></a>
                  <br>
                  <?php the_content(); ?>
              </div>

        </div>
    </section>
        
        <?php
            }
        ?>
    
   
	<?php get_footer();?>

  </body>
</html>