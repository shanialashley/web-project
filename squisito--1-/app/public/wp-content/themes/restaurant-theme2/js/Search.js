// class Search{     
//     constructor(){        
//          alert("This is a JavaScript search");     
//     } 
// } 
// export default Search; 

import $ from 'jquery';
class Search{
    // init objects
    constructor(){
        //leveraging JQuery libray
        this.openButton = $(".js-search-trigger");
        this.closeButton = $(".js-search-overlay__close");
        this.searchOverlay = $(".search-overlay");
        this.events();
    }

    //events
    events(){
        this.openButton.on("click", this.openOverlay.bind(this));
        this.closeButton.on("click", this.closeOverlay.bind(this));
    }
    //open overlay
    openOverlay(){
        this.searchOverlay.addClass("search-overlay--active");
    }
    //close overlay
    closeOverlay(){
        this.searchOverlay.removeClass("search-overlay--active");
    }

}
export default Search;