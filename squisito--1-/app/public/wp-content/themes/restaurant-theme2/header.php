<!DOCTYPE html>
<html>
 <head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale =1">
		
 		<?php wp_head();?>
 </head> 

 <body>
 <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
		      <a class="navbar-brand" href="index.html"><span class="flaticon-pizza-1 mr-1"></span>Squisito<br><small>Restaurant</small></a>
		      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="oi oi-menu"></span> Menu
		      </button>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav ml-auto">
				<li class="nav-item"><a href="<?php echo site_url() ?>" class="nav-link">Home</a></li>
			  
				<li class="nav-item" <?php if(get_post_type() == 'menu')  ?>>
					<a href="<?php echo get_post_type_archive_link('menu'); ?>" class="nav-link" >Menu</a></li>
				
				<li class="nav-item" <?php if(get_post_type() == 'deal')  ?>>
				<a href="<?php echo get_post_type_archive_link('deal'); ?>" class="nav-link" >Deals</a></li>
				
				<li class="nav-item" <?php if(get_post_type() == 'review')  ?>>
               	 <a href="<?php echo get_post_type_archive_link('review'); ?>" class="nav-link" >Reviews</a></li>

			  <li class= "nav-item"<?php if(get_post_type() == 'post') ?>>
				  <a href="<?php echo site_url('/blog'); ?>" class="nav-link" >Blog</a>
			  </li>
			  
			  <li class="nav-item"> <a href="<?php echo site_url('/about')?>" class="nav-link">About</a>

			  <li class="nav-item"> <a href="<?php echo site_url('/contact')?>" class="nav-link">Contact</a>

			  <li class="nav-item">
				<?php
				if(is_user_logged_in()){ ?>   
				<li class="nav-item">           
					<a href="<?php echo wp_logout_url(); ?>" class="nav-link">Log Out</span>               
					</a>
					</li>             
					<?php }             
				else{ ?>
				<li class="nav-item"><a href="<?php echo wp_login_url(); ?>" class="nav-link">Login</a>
				<li class="nav-item"><a href="<?php echo esc_url(site_url('/wp-signup.php'));  ?>" class="nav-link">Sign Up</a>
						<span class="search-trigger js-search-trigger"><i class="fa fa-search" aria-hidden="true"></i></span>
				<?php }
						?>
			  
	        </ul>
	      </div>
		  </div>
	  </nav>
</body>

</html>
