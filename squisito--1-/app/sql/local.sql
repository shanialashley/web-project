-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: ::1    Database: local
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wp_commentmeta`
--

DROP TABLE IF EXISTS `wp_commentmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_commentmeta`
--

LOCK TABLES `wp_commentmeta` WRITE;
/*!40000 ALTER TABLE `wp_commentmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_commentmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_comments`
--

DROP TABLE IF EXISTS `wp_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_comments`
--

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;
INSERT INTO `wp_comments` VALUES (1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2020-03-27 20:18:46','2020-03-27 20:18:46','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'post-trashed','','',0,0);
/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_links`
--

DROP TABLE IF EXISTS `wp_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_links`
--

LOCK TABLES `wp_links` WRITE;
/*!40000 ALTER TABLE `wp_links` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_options`
--

DROP TABLE IF EXISTS `wp_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`),
  KEY `autoload` (`autoload`)
) ENGINE=InnoDB AUTO_INCREMENT=462 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_options`
--

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;
INSERT INTO `wp_options` VALUES (1,'siteurl','http://squisito-1.local','yes');
INSERT INTO `wp_options` VALUES (2,'home','http://squisito-1.local','yes');
INSERT INTO `wp_options` VALUES (3,'blogname','Squisito','yes');
INSERT INTO `wp_options` VALUES (4,'blogdescription','Just another WordPress site','yes');
INSERT INTO `wp_options` VALUES (5,'users_can_register','1','yes');
INSERT INTO `wp_options` VALUES (6,'admin_email','chanelle.mondesir@my.uwi.edu','yes');
INSERT INTO `wp_options` VALUES (7,'start_of_week','1','yes');
INSERT INTO `wp_options` VALUES (8,'use_balanceTags','0','yes');
INSERT INTO `wp_options` VALUES (9,'use_smilies','1','yes');
INSERT INTO `wp_options` VALUES (10,'require_name_email','1','yes');
INSERT INTO `wp_options` VALUES (11,'comments_notify','1','yes');
INSERT INTO `wp_options` VALUES (12,'posts_per_rss','10','yes');
INSERT INTO `wp_options` VALUES (13,'rss_use_excerpt','0','yes');
INSERT INTO `wp_options` VALUES (14,'mailserver_url','mail.example.com','yes');
INSERT INTO `wp_options` VALUES (15,'mailserver_login','login@example.com','yes');
INSERT INTO `wp_options` VALUES (16,'mailserver_pass','password','yes');
INSERT INTO `wp_options` VALUES (17,'mailserver_port','110','yes');
INSERT INTO `wp_options` VALUES (18,'default_category','1','yes');
INSERT INTO `wp_options` VALUES (19,'default_comment_status','open','yes');
INSERT INTO `wp_options` VALUES (20,'default_ping_status','open','yes');
INSERT INTO `wp_options` VALUES (21,'default_pingback_flag','1','yes');
INSERT INTO `wp_options` VALUES (22,'posts_per_page','10','yes');
INSERT INTO `wp_options` VALUES (23,'date_format','F j, Y','yes');
INSERT INTO `wp_options` VALUES (24,'time_format','g:i a','yes');
INSERT INTO `wp_options` VALUES (25,'links_updated_date_format','F j, Y g:i a','yes');
INSERT INTO `wp_options` VALUES (26,'comment_moderation','0','yes');
INSERT INTO `wp_options` VALUES (27,'moderation_notify','1','yes');
INSERT INTO `wp_options` VALUES (28,'permalink_structure','/%postname%/','yes');
INSERT INTO `wp_options` VALUES (29,'rewrite_rules','a:158:{s:7:\"menu/?$\";s:24:\"index.php?post_type=menu\";s:37:\"menu/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=menu&feed=$matches[1]\";s:32:\"menu/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=menu&feed=$matches[1]\";s:24:\"menu/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=menu&paged=$matches[1]\";s:7:\"deal/?$\";s:24:\"index.php?post_type=deal\";s:37:\"deal/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=deal&feed=$matches[1]\";s:32:\"deal/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=deal&feed=$matches[1]\";s:24:\"deal/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=deal&paged=$matches[1]\";s:9:\"review/?$\";s:26:\"index.php?post_type=review\";s:39:\"review/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=review&feed=$matches[1]\";s:34:\"review/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?post_type=review&feed=$matches[1]\";s:26:\"review/page/([0-9]{1,})/?$\";s:44:\"index.php?post_type=review&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:32:\"menu/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"menu/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"menu/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"menu/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"menu/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"menu/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"menu/([^/]+)/embed/?$\";s:37:\"index.php?menu=$matches[1]&embed=true\";s:25:\"menu/([^/]+)/trackback/?$\";s:31:\"index.php?menu=$matches[1]&tb=1\";s:45:\"menu/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?menu=$matches[1]&feed=$matches[2]\";s:40:\"menu/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?menu=$matches[1]&feed=$matches[2]\";s:33:\"menu/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?menu=$matches[1]&paged=$matches[2]\";s:40:\"menu/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?menu=$matches[1]&cpage=$matches[2]\";s:29:\"menu/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?menu=$matches[1]&page=$matches[2]\";s:21:\"menu/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"menu/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"menu/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"menu/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"menu/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"menu/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"deal/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"deal/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"deal/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"deal/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"deal/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"deal/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"deal/([^/]+)/embed/?$\";s:37:\"index.php?deal=$matches[1]&embed=true\";s:25:\"deal/([^/]+)/trackback/?$\";s:31:\"index.php?deal=$matches[1]&tb=1\";s:45:\"deal/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?deal=$matches[1]&feed=$matches[2]\";s:40:\"deal/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?deal=$matches[1]&feed=$matches[2]\";s:33:\"deal/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?deal=$matches[1]&paged=$matches[2]\";s:40:\"deal/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?deal=$matches[1]&cpage=$matches[2]\";s:29:\"deal/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?deal=$matches[1]&page=$matches[2]\";s:21:\"deal/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"deal/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"deal/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"deal/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"deal/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"deal/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"review/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"review/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"review/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"review/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"review/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"review/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:23:\"review/([^/]+)/embed/?$\";s:39:\"index.php?review=$matches[1]&embed=true\";s:27:\"review/([^/]+)/trackback/?$\";s:33:\"index.php?review=$matches[1]&tb=1\";s:47:\"review/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?review=$matches[1]&feed=$matches[2]\";s:42:\"review/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?review=$matches[1]&feed=$matches[2]\";s:35:\"review/([^/]+)/page/?([0-9]{1,})/?$\";s:46:\"index.php?review=$matches[1]&paged=$matches[2]\";s:42:\"review/([^/]+)/comment-page-([0-9]{1,})/?$\";s:46:\"index.php?review=$matches[1]&cpage=$matches[2]\";s:31:\"review/([^/]+)(?:/([0-9]+))?/?$\";s:45:\"index.php?review=$matches[1]&page=$matches[2]\";s:23:\"review/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:33:\"review/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:53:\"review/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"review/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:48:\"review/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:29:\"review/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=12&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes');
INSERT INTO `wp_options` VALUES (30,'hack_file','0','yes');
INSERT INTO `wp_options` VALUES (31,'blog_charset','UTF-8','yes');
INSERT INTO `wp_options` VALUES (32,'moderation_keys','','no');
INSERT INTO `wp_options` VALUES (33,'active_plugins','a:5:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";i:2;s:35:\"crop-thumbnails/crop-thumbnails.php\";i:3;s:19:\"members/members.php\";i:4;s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";}','yes');
INSERT INTO `wp_options` VALUES (34,'category_base','','yes');
INSERT INTO `wp_options` VALUES (35,'ping_sites','http://rpc.pingomatic.com/','yes');
INSERT INTO `wp_options` VALUES (36,'comment_max_links','2','yes');
INSERT INTO `wp_options` VALUES (37,'gmt_offset','0','yes');
INSERT INTO `wp_options` VALUES (38,'default_email_category','1','yes');
INSERT INTO `wp_options` VALUES (39,'recently_edited','','no');
INSERT INTO `wp_options` VALUES (40,'template','restaurant-theme2','yes');
INSERT INTO `wp_options` VALUES (41,'stylesheet','restaurant-theme2','yes');
INSERT INTO `wp_options` VALUES (42,'comment_whitelist','1','yes');
INSERT INTO `wp_options` VALUES (43,'blacklist_keys','','no');
INSERT INTO `wp_options` VALUES (44,'comment_registration','0','yes');
INSERT INTO `wp_options` VALUES (45,'html_type','text/html','yes');
INSERT INTO `wp_options` VALUES (46,'use_trackback','0','yes');
INSERT INTO `wp_options` VALUES (47,'default_role','subscriber','yes');
INSERT INTO `wp_options` VALUES (48,'db_version','47018','yes');
INSERT INTO `wp_options` VALUES (49,'uploads_use_yearmonth_folders','1','yes');
INSERT INTO `wp_options` VALUES (50,'upload_path','','yes');
INSERT INTO `wp_options` VALUES (51,'blog_public','1','yes');
INSERT INTO `wp_options` VALUES (52,'default_link_category','2','yes');
INSERT INTO `wp_options` VALUES (53,'show_on_front','page','yes');
INSERT INTO `wp_options` VALUES (54,'tag_base','','yes');
INSERT INTO `wp_options` VALUES (55,'show_avatars','1','yes');
INSERT INTO `wp_options` VALUES (56,'avatar_rating','G','yes');
INSERT INTO `wp_options` VALUES (57,'upload_url_path','','yes');
INSERT INTO `wp_options` VALUES (58,'thumbnail_size_w','150','yes');
INSERT INTO `wp_options` VALUES (59,'thumbnail_size_h','150','yes');
INSERT INTO `wp_options` VALUES (60,'thumbnail_crop','1','yes');
INSERT INTO `wp_options` VALUES (61,'medium_size_w','300','yes');
INSERT INTO `wp_options` VALUES (62,'medium_size_h','300','yes');
INSERT INTO `wp_options` VALUES (63,'avatar_default','mystery','yes');
INSERT INTO `wp_options` VALUES (64,'large_size_w','1024','yes');
INSERT INTO `wp_options` VALUES (65,'large_size_h','1024','yes');
INSERT INTO `wp_options` VALUES (66,'image_default_link_type','none','yes');
INSERT INTO `wp_options` VALUES (67,'image_default_size','','yes');
INSERT INTO `wp_options` VALUES (68,'image_default_align','','yes');
INSERT INTO `wp_options` VALUES (69,'close_comments_for_old_posts','0','yes');
INSERT INTO `wp_options` VALUES (70,'close_comments_days_old','14','yes');
INSERT INTO `wp_options` VALUES (71,'thread_comments','1','yes');
INSERT INTO `wp_options` VALUES (72,'thread_comments_depth','5','yes');
INSERT INTO `wp_options` VALUES (73,'page_comments','0','yes');
INSERT INTO `wp_options` VALUES (74,'comments_per_page','50','yes');
INSERT INTO `wp_options` VALUES (75,'default_comments_page','newest','yes');
INSERT INTO `wp_options` VALUES (76,'comment_order','asc','yes');
INSERT INTO `wp_options` VALUES (77,'sticky_posts','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (79,'widget_text','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (80,'widget_rss','a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (81,'uninstall_plugins','a:0:{}','no');
INSERT INTO `wp_options` VALUES (82,'timezone_string','','yes');
INSERT INTO `wp_options` VALUES (83,'page_for_posts','14','yes');
INSERT INTO `wp_options` VALUES (84,'page_on_front','12','yes');
INSERT INTO `wp_options` VALUES (85,'default_post_format','0','yes');
INSERT INTO `wp_options` VALUES (86,'link_manager_enabled','0','yes');
INSERT INTO `wp_options` VALUES (87,'finished_splitting_shared_terms','1','yes');
INSERT INTO `wp_options` VALUES (88,'site_icon','0','yes');
INSERT INTO `wp_options` VALUES (89,'medium_large_size_w','768','yes');
INSERT INTO `wp_options` VALUES (90,'medium_large_size_h','0','yes');
INSERT INTO `wp_options` VALUES (91,'wp_page_for_privacy_policy','3','yes');
INSERT INTO `wp_options` VALUES (92,'show_comments_cookies_opt_in','1','yes');
INSERT INTO `wp_options` VALUES (93,'admin_email_lifespan','1600892325','yes');
INSERT INTO `wp_options` VALUES (94,'initial_db_version','45805','yes');
INSERT INTO `wp_options` VALUES (95,'wp_user_roles','a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:96:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:16:\"restrict_content\";b:1;s:10:\"list_roles\";b:1;s:12:\"create_roles\";b:1;s:12:\"delete_roles\";b:1;s:10:\"edit_roles\";b:1;s:10:\"edit_menus\";b:1;s:17:\"edit_others_menus\";b:1;s:12:\"delete_menus\";b:1;s:13:\"publish_menus\";b:1;s:18:\"read_private_menus\";b:1;s:20:\"delete_private_menus\";b:1;s:22:\"delete_published_menus\";b:1;s:19:\"delete_others_menus\";b:1;s:18:\"edit_private_menus\";b:1;s:20:\"edit_published_menus\";b:1;s:10:\"edit_deals\";b:1;s:17:\"edit_others_deals\";b:1;s:12:\"delete_deals\";b:1;s:13:\"publish_deals\";b:1;s:18:\"read_private_deals\";b:1;s:20:\"delete_private_deals\";b:1;s:22:\"delete_published_deals\";b:1;s:19:\"delete_others_deals\";b:1;s:18:\"edit_private_deals\";b:1;s:20:\"edit_published_deals\";b:1;s:12:\"edit_reviews\";b:1;s:19:\"edit_others_reviews\";b:1;s:14:\"delete_reviews\";b:1;s:15:\"publish_reviews\";b:1;s:20:\"read_private_reviews\";b:1;s:22:\"delete_private_reviews\";b:1;s:24:\"delete_published_reviews\";b:1;s:21:\"delete_others_reviews\";b:1;s:20:\"edit_private_reviews\";b:1;s:22:\"edit_published_reviews\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:12:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;s:21:\"delete_others_reviews\";b:1;s:22:\"delete_private_reviews\";b:1;s:24:\"delete_published_reviews\";b:1;s:14:\"delete_reviews\";b:1;s:19:\"edit_others_reviews\";b:1;s:20:\"edit_private_reviews\";b:1;s:22:\"edit_published_reviews\";b:1;s:12:\"edit_reviews\";b:1;s:15:\"publish_reviews\";b:1;s:20:\"read_private_reviews\";b:1;}}s:12:\"menu_editors\";a:2:{s:4:\"name\";s:12:\"Menu Editors\";s:12:\"capabilities\";a:11:{s:12:\"delete_menus\";b:1;s:19:\"delete_others_menus\";b:1;s:20:\"delete_private_menus\";b:1;s:22:\"delete_published_menus\";b:1;s:10:\"edit_menus\";b:1;s:17:\"edit_others_menus\";b:1;s:18:\"edit_private_menus\";b:1;s:20:\"edit_published_menus\";b:1;s:13:\"publish_menus\";b:1;s:4:\"read\";b:1;s:18:\"read_private_menus\";b:1;}}s:12:\"deal_planner\";a:2:{s:4:\"name\";s:12:\"Deal Planner\";s:12:\"capabilities\";a:11:{s:12:\"delete_deals\";b:1;s:19:\"delete_others_deals\";b:1;s:20:\"delete_private_deals\";b:1;s:22:\"delete_published_deals\";b:1;s:10:\"edit_deals\";b:1;s:17:\"edit_others_deals\";b:1;s:18:\"edit_private_deals\";b:1;s:20:\"edit_published_deals\";b:1;s:13:\"publish_deals\";b:1;s:4:\"read\";b:1;s:18:\"read_private_deals\";b:1;}}}','yes');
INSERT INTO `wp_options` VALUES (96,'fresh_site','0','yes');
INSERT INTO `wp_options` VALUES (97,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (98,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (99,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (100,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (101,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (102,'sidebars_widgets','a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes');
INSERT INTO `wp_options` VALUES (103,'cron','a:8:{i:1587521929;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1587543529;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1587586727;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1587587639;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1587587640;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1587596259;a:1:{s:21:\"ai1wm_storage_cleanup\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1588030095;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}','yes');
INSERT INTO `wp_options` VALUES (104,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (105,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (106,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (107,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (108,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (109,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (110,'nonce_key','T-<SHB3ST:Y|]iFGQ7l|Wfn;8f y10H>BB-Q(S9X:[U[R:JoaB(sP;eztnf7OoHj','no');
INSERT INTO `wp_options` VALUES (111,'nonce_salt','L[gvu$5.;1)CUqE/J?anY(<cWaa|51(|.,_q DZxz|bxiNd;hx]x/w*KP9oK}fI7','no');
INSERT INTO `wp_options` VALUES (112,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (113,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (114,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (116,'recovery_keys','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (123,'theme_mods_twentytwenty','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1585343471;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (146,'theme_mods_restaurant-theme','a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586560572;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes');
INSERT INTO `wp_options` VALUES (147,'current_theme','Pizza','yes');
INSERT INTO `wp_options` VALUES (148,'theme_switched','','yes');
INSERT INTO `wp_options` VALUES (229,'theme_mods_restaurant-theme2','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}','yes');
INSERT INTO `wp_options` VALUES (231,'recovery_mode_email_last_sent','1586704391','yes');
INSERT INTO `wp_options` VALUES (267,'_site_transient_timeout_browser_7882d5a5641293bd72253bfbda0286f6','1587941538','no');
INSERT INTO `wp_options` VALUES (268,'_site_transient_browser_7882d5a5641293bd72253bfbda0286f6','a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"80.0.3987.163\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}','no');
INSERT INTO `wp_options` VALUES (269,'_site_transient_timeout_php_check_472f71d2a071d463a95f84346288dc89','1587941539','no');
INSERT INTO `wp_options` VALUES (270,'_site_transient_php_check_472f71d2a071d463a95f84346288dc89','a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}','no');
INSERT INTO `wp_options` VALUES (292,'recently_activated','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (299,'ai1wm_secret_key','92YuTpF4blUJ','yes');
INSERT INTO `wp_options` VALUES (300,'ai1wm_status','a:2:{s:4:\"type\";s:8:\"download\";s:7:\"message\";s:317:\"<a href=\"http://squisito-1.local/wp-content/ai1wm-backups/squisito-1.local-20200421-045955-463eeo.wpress\" class=\"ai1wm-button-green ai1wm-emphasize ai1wm-button-download\" title=\"squisito-1.local\" download=\"squisito-1.local-20200421-045955-463eeo.wpress\"><span>Download squisito-1.local</span><em>Size: 125 MB</em></a>\";}','yes');
INSERT INTO `wp_options` VALUES (307,'db_upgraded','','yes');
INSERT INTO `wp_options` VALUES (309,'ai1wm_updater','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (312,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:57:\"https://downloads.wordpress.org/release/wordpress-5.4.zip\";s:10:\"no_content\";s:68:\"https://downloads.wordpress.org/release/wordpress-5.4-no-content.zip\";s:11:\"new_bundled\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.4-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:3:\"5.4\";s:7:\"version\";s:3:\"5.4\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1587519138;s:15:\"version_checked\";s:3:\"5.4\";s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (316,'can_compress_scripts','0','no');
INSERT INTO `wp_options` VALUES (346,'acf_version','5.8.9','yes');
INSERT INTO `wp_options` VALUES (357,'WPLANG','','yes');
INSERT INTO `wp_options` VALUES (358,'new_admin_email','chanelle.mondesir@my.uwi.edu','yes');
INSERT INTO `wp_options` VALUES (408,'members_addons_migrated','1','yes');
INSERT INTO `wp_options` VALUES (409,'widget_members-widget-login','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (410,'widget_members-widget-users','a:1:{s:12:\"_multiwidget\";i:1;}','yes');
INSERT INTO `wp_options` VALUES (432,'category_children','a:0:{}','yes');
INSERT INTO `wp_options` VALUES (438,'_transient_health-check-site-status-result','{\"good\":\"10\",\"recommended\":\"7\",\"critical\":\"0\"}','yes');
INSERT INTO `wp_options` VALUES (458,'_site_transient_timeout_theme_roots','1587520939','no');
INSERT INTO `wp_options` VALUES (459,'_site_transient_theme_roots','a:5:{s:17:\"restaurant-theme2\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";}','no');
INSERT INTO `wp_options` VALUES (460,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1587519143;s:7:\"checked\";a:5:{s:17:\"restaurant-theme2\";s:3:\"1.0\";s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.1\";}s:8:\"response\";a:4:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.5.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.3.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.1.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:12:\"twentytwenty\";a:6:{s:5:\"theme\";s:12:\"twentytwenty\";s:11:\"new_version\";s:3:\"1.2\";s:3:\"url\";s:42:\"https://wordpress.org/themes/twentytwenty/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/theme/twentytwenty.1.2.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}','no');
INSERT INTO `wp_options` VALUES (461,'_site_transient_update_plugins','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1587519144;s:8:\"response\";a:2:{s:19:\"members/members.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/members\";s:4:\"slug\";s:7:\"members\";s:6:\"plugin\";s:19:\"members/members.php\";s:11:\"new_version\";s:5:\"3.0.4\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/members/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/members.3.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:60:\"https://ps.w.org/members/assets/icon-256x256.png?rev=2126126\";s:2:\"1x\";s:60:\"https://ps.w.org/members/assets/icon-128x128.png?rev=2126126\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/members/assets/banner-1544x500.png?rev=2126126\";s:2:\"1x\";s:62:\"https://ps.w.org/members/assets/banner-772x250.png?rev=2126126\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:3:\"5.4\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:70:\"w.org/plugins/custom-registration-form-builder-with-submission-manager\";s:4:\"slug\";s:56:\"custom-registration-form-builder-with-submission-manager\";s:6:\"plugin\";s:79:\"custom-registration-form-builder-with-submission-manager/registration_magic.php\";s:11:\"new_version\";s:7:\"4.6.0.7\";s:3:\"url\";s:87:\"https://wordpress.org/plugins/custom-registration-form-builder-with-submission-manager/\";s:7:\"package\";s:99:\"https://downloads.wordpress.org/plugin/custom-registration-form-builder-with-submission-manager.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:109:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/icon-256x256.png?rev=2049638\";s:2:\"1x\";s:109:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/icon-128x128.png?rev=2049638\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:111:\"https://ps.w.org/custom-registration-form-builder-with-submission-manager/assets/banner-772x250.jpg?rev=2081490\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:257:\"<ul>\n<li>Fixed Translation Issue with Inbox Property Filters (Premium Only)</li>\n<li>Fixed Provisional Login with Activation Link (Premium Only)</li>\n<li>Fixed Lost Password Link on Login Form &amp; Widgets</li>\n<li>Fixed Login Form Design Issues</li>\n</ul>\";s:6:\"tested\";s:5:\"5.3.2\";s:12:\"requires_php\";s:3:\"5.6\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:6:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.9\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:37:\"w.org/plugins/all-in-one-wp-migration\";s:4:\"slug\";s:23:\"all-in-one-wp-migration\";s:6:\"plugin\";s:51:\"all-in-one-wp-migration/all-in-one-wp-migration.php\";s:11:\"new_version\";s:4:\"7.20\";s:3:\"url\";s:54:\"https://wordpress.org/plugins/all-in-one-wp-migration/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.20.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-256x256.png?rev=2246309\";s:2:\"1x\";s:76:\"https://ps.w.org/all-in-one-wp-migration/assets/icon-128x128.png?rev=2246309\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:79:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-1544x500.png?rev=2246309\";s:2:\"1x\";s:78:\"https://ps.w.org/all-in-one-wp-migration/assets/banner-772x250.png?rev=2246309\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"crop-thumbnails/crop-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/crop-thumbnails\";s:4:\"slug\";s:15:\"crop-thumbnails\";s:6:\"plugin\";s:35:\"crop-thumbnails/crop-thumbnails.php\";s:11:\"new_version\";s:5:\"1.2.6\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/crop-thumbnails/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/crop-thumbnails.1.2.6.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:60:\"https://ps.w.org/crop-thumbnails/assets/icon.svg?rev=1228698\";s:3:\"svg\";s:60:\"https://ps.w.org/crop-thumbnails/assets/icon.svg?rev=1228698\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/crop-thumbnails/assets/banner-1544x500.jpg?rev=626571\";s:2:\"1x\";s:69:\"https://ps.w.org/crop-thumbnails/assets/banner-772x250.jpg?rev=626571\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"dashicons-cpt/dashicons-cpt.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/dashicons-cpt\";s:4:\"slug\";s:13:\"dashicons-cpt\";s:6:\"plugin\";s:31:\"dashicons-cpt/dashicons-cpt.php\";s:11:\"new_version\";s:5:\"1.0.2\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/dashicons-cpt/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/dashicons-cpt.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/dashicons-cpt/assets/icon-256x256.png?rev=1081988\";s:2:\"1x\";s:66:\"https://ps.w.org/dashicons-cpt/assets/icon-128x128.png?rev=1081988\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/dashicons-cpt/assets/banner-772x250.jpg?rev=1081983\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"rate-my-post/rate-my-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/rate-my-post\";s:4:\"slug\";s:12:\"rate-my-post\";s:6:\"plugin\";s:29:\"rate-my-post/rate-my-post.php\";s:11:\"new_version\";s:5:\"3.1.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/rate-my-post/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/rate-my-post.3.1.0.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/rate-my-post/assets/icon-128x128.png?rev=2045796\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/rate-my-post/assets/banner-772x250.png?rev=2045796\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/regenerate-thumbnails\";s:4:\"slug\";s:21:\"regenerate-thumbnails\";s:6:\"plugin\";s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";s:11:\"new_version\";s:5:\"3.1.3\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/regenerate-thumbnails/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/regenerate-thumbnails.3.1.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:74:\"https://ps.w.org/regenerate-thumbnails/assets/icon-128x128.png?rev=1753390\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/regenerate-thumbnails/assets/banner-1544x500.jpg?rev=1753390\";s:2:\"1x\";s:76:\"https://ps.w.org/regenerate-thumbnails/assets/banner-772x250.jpg?rev=1753390\";}s:11:\"banners_rtl\";a:0:{}}}}','no');
/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_postmeta`
--

DROP TABLE IF EXISTS `wp_postmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_postmeta`
--

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;
INSERT INTO `wp_postmeta` VALUES (1,2,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (2,3,'_wp_page_template','default');
INSERT INTO `wp_postmeta` VALUES (3,5,'_edit_lock','1586645822:1');
INSERT INTO `wp_postmeta` VALUES (4,8,'_edit_lock','1586561401:1');
INSERT INTO `wp_postmeta` VALUES (5,10,'_edit_lock','1587336843:1');
INSERT INTO `wp_postmeta` VALUES (6,12,'_edit_lock','1586604757:1');
INSERT INTO `wp_postmeta` VALUES (7,14,'_edit_lock','1587372632:1');
INSERT INTO `wp_postmeta` VALUES (8,16,'_edit_lock','1587365403:1');
INSERT INTO `wp_postmeta` VALUES (11,16,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (14,19,'_edit_lock','1586645856:1');
INSERT INTO `wp_postmeta` VALUES (15,21,'_edit_lock','1586645960:1');
INSERT INTO `wp_postmeta` VALUES (16,24,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (17,24,'_edit_lock','1587393436:1');
INSERT INTO `wp_postmeta` VALUES (18,25,'_wp_attached_file','2020/04/pizza-deal1.jpg');
INSERT INTO `wp_postmeta` VALUES (19,25,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:285;s:6:\"height\";i:177;s:4:\"file\";s:23:\"2020/04/pizza-deal1.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"pizza-deal1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:21:\"pizza-deal1-50x50.jpg\";s:5:\"width\";i:50;s:6:\"height\";i:50;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (22,28,'_wp_attached_file','2020/04/Opening.jpg');
INSERT INTO `wp_postmeta` VALUES (23,28,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:267;s:6:\"height\";i:189;s:4:\"file\";s:19:\"2020/04/Opening.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"Opening-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:17:\"Opening-50x50.jpg\";s:5:\"width\";i:50;s:6:\"height\";i:50;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (28,16,'_thumbnail_id','30');
INSERT INTO `wp_postmeta` VALUES (29,30,'_wp_attached_file','2020/04/opening-1.jpg');
INSERT INTO `wp_postmeta` VALUES (30,30,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:626;s:6:\"height\";i:417;s:4:\"file\";s:21:\"2020/04/opening-1.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"opening-1-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"opening-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:19:\"opening-1-50x50.jpg\";s:5:\"width\";i:50;s:6:\"height\";i:50;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (33,1,'_edit_lock','1587364929:1');
INSERT INTO `wp_postmeta` VALUES (34,1,'_wp_trash_meta_status','private');
INSERT INTO `wp_postmeta` VALUES (35,1,'_wp_trash_meta_time','1587365077');
INSERT INTO `wp_postmeta` VALUES (36,1,'_wp_desired_post_slug','hello-world');
INSERT INTO `wp_postmeta` VALUES (37,1,'_wp_trash_meta_comments_status','a:1:{i:1;s:1:\"1\";}');
INSERT INTO `wp_postmeta` VALUES (38,33,'_edit_lock','1587365200:1');
INSERT INTO `wp_postmeta` VALUES (47,39,'_wp_attached_file','2020/04/open.jpg');
INSERT INTO `wp_postmeta` VALUES (48,39,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:852;s:6:\"height\";i:480;s:4:\"file\";s:16:\"2020/04/open.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"open-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"open-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"open-768x433.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:433;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:14:\"open-50x50.jpg\";s:5:\"width\";i:50;s:6:\"height\";i:50;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (51,33,'_thumbnail_id','39');
INSERT INTO `wp_postmeta` VALUES (54,42,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (55,42,'_edit_lock','1587369014:1');
INSERT INTO `wp_postmeta` VALUES (56,43,'_wp_attached_file','2020/04/p-1.jpg');
INSERT INTO `wp_postmeta` VALUES (57,43,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:442;s:6:\"height\";i:450;s:4:\"file\";s:15:\"2020/04/p-1.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p-1-295x300.jpg\";s:5:\"width\";i:295;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"post-thumbnail\";a:4:{s:4:\"file\";s:13:\"p-1-50x50.jpg\";s:5:\"width\";i:50;s:6:\"height\";i:50;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (58,42,'_thumbnail_id','43');
INSERT INTO `wp_postmeta` VALUES (59,8,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (60,8,'_wp_trash_meta_time','1587368475');
INSERT INTO `wp_postmeta` VALUES (61,8,'_wp_desired_post_slug','menu');
INSERT INTO `wp_postmeta` VALUES (62,42,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (63,42,'_wp_trash_meta_time','1587369229');
INSERT INTO `wp_postmeta` VALUES (64,42,'_wp_desired_post_slug','pizza-veronese');
INSERT INTO `wp_postmeta` VALUES (65,44,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (66,44,'_edit_lock','1587370660:1');
INSERT INTO `wp_postmeta` VALUES (67,47,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (68,47,'_edit_lock','1587393457:1');
INSERT INTO `wp_postmeta` VALUES (69,49,'_edit_lock','1587372606:1');
INSERT INTO `wp_postmeta` VALUES (70,49,'_wp_trash_meta_status','publish');
INSERT INTO `wp_postmeta` VALUES (71,49,'_wp_trash_meta_time','1587372782');
INSERT INTO `wp_postmeta` VALUES (72,49,'_wp_desired_post_slug','menu');
INSERT INTO `wp_postmeta` VALUES (73,51,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (74,51,'_thumbnail_id','43');
INSERT INTO `wp_postmeta` VALUES (75,51,'related_deals','');
INSERT INTO `wp_postmeta` VALUES (76,51,'_related_deals','field_5e9d5b5a4282e');
INSERT INTO `wp_postmeta` VALUES (77,51,'_edit_lock','1587386999:1');
INSERT INTO `wp_postmeta` VALUES (78,52,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (79,52,'_edit_lock','1587423284:1');
INSERT INTO `wp_postmeta` VALUES (80,52,'_thumbnail_id','43');
INSERT INTO `wp_postmeta` VALUES (81,24,'_thumbnail_id','25');
INSERT INTO `wp_postmeta` VALUES (82,24,'deals_date','20200413');
INSERT INTO `wp_postmeta` VALUES (83,24,'_deals_date','field_5e9d5abde65bc');
INSERT INTO `wp_postmeta` VALUES (84,24,'_','field_5e9d5b0be65bd');
INSERT INTO `wp_postmeta` VALUES (85,57,'_edit_lock','1587396349:2');
INSERT INTO `wp_postmeta` VALUES (86,57,'_edit_last','2');
INSERT INTO `wp_postmeta` VALUES (87,58,'_edit_lock','1587423249:1');
INSERT INTO `wp_postmeta` VALUES (88,58,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (89,59,'_wp_attached_file','2020/04/p-2.jpg');
INSERT INTO `wp_postmeta` VALUES (90,59,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:235;s:6:\"height\";i:214;s:4:\"file\";s:15:\"2020/04/p-2.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (91,58,'_thumbnail_id','59');
INSERT INTO `wp_postmeta` VALUES (92,61,'_edit_lock','1587423235:1');
INSERT INTO `wp_postmeta` VALUES (93,61,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (94,62,'_wp_attached_file','2020/04/p-3.jpg');
INSERT INTO `wp_postmeta` VALUES (95,62,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1500;s:6:\"height\";i:1000;s:4:\"file\";s:15:\"2020/04/p-3.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p-3-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p-3-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"p-3-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"p-3-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"p-3-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (96,61,'_thumbnail_id','62');
INSERT INTO `wp_postmeta` VALUES (97,63,'_edit_lock','1587423186:1');
INSERT INTO `wp_postmeta` VALUES (98,63,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (99,64,'_wp_attached_file','2020/04/pizza-7-1.jpg');
INSERT INTO `wp_postmeta` VALUES (100,64,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1233;s:6:\"height\";i:1175;s:4:\"file\";s:21:\"2020/04/pizza-7-1.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"pizza-7-1-300x286.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:286;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"pizza-7-1-1024x976.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:976;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"pizza-7-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"pizza-7-1-768x732.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:732;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:22:\"pizza-7-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:22:\"pizza-7-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (101,63,'_thumbnail_id','64');
INSERT INTO `wp_postmeta` VALUES (102,65,'_edit_lock','1587423140:1');
INSERT INTO `wp_postmeta` VALUES (103,65,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (104,66,'_wp_attached_file','2020/04/p-4.jpg');
INSERT INTO `wp_postmeta` VALUES (105,66,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:640;s:6:\"height\";i:640;s:4:\"file\";s:15:\"2020/04/p-4.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p-4-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (106,65,'_thumbnail_id','66');
INSERT INTO `wp_postmeta` VALUES (107,67,'_edit_lock','1587423124:1');
INSERT INTO `wp_postmeta` VALUES (108,67,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (109,68,'_wp_attached_file','2020/04/pizza-13-2.jpg');
INSERT INTO `wp_postmeta` VALUES (110,68,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:1067;s:4:\"file\";s:22:\"2020/04/pizza-13-2.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"pizza-13-2-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"pizza-13-2-1024x683.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:683;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"pizza-13-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"pizza-13-2-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:24:\"pizza-13-2-1536x1024.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:23:\"pizza-13-2-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:23:\"pizza-13-2-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (111,67,'_thumbnail_id','68');
INSERT INTO `wp_postmeta` VALUES (112,69,'_edit_lock','1587440371:1');
INSERT INTO `wp_postmeta` VALUES (113,69,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (114,70,'_wp_attached_file','2020/04/p-5.png');
INSERT INTO `wp_postmeta` VALUES (115,70,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:900;s:4:\"file\";s:15:\"2020/04/p-5.png\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p-5-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p-5-1024x768.png\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"p-5-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"p-5-1000x660.png\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"p-5-1000x660.png\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (116,69,'_thumbnail_id','70');
INSERT INTO `wp_postmeta` VALUES (117,71,'_edit_lock','1587423084:1');
INSERT INTO `wp_postmeta` VALUES (118,71,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (119,72,'_wp_attached_file','2020/04/p-6.jpg');
INSERT INTO `wp_postmeta` VALUES (120,72,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:720;s:4:\"file\";s:15:\"2020/04/p-6.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"p-6-300x180.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"p-6-1024x614.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:614;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"p-6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"p-6-768x461.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:461;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"p-6-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"p-6-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (121,71,'_thumbnail_id','72');
INSERT INTO `wp_postmeta` VALUES (122,74,'_edit_lock','1587444450:1');
INSERT INTO `wp_postmeta` VALUES (123,74,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (124,75,'_wp_attached_file','2020/04/drink-1.jpg');
INSERT INTO `wp_postmeta` VALUES (125,75,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2020/04/drink-1.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"drink-1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-1-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-1-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (126,74,'_thumbnail_id','75');
INSERT INTO `wp_postmeta` VALUES (127,76,'_edit_lock','1587429819:1');
INSERT INTO `wp_postmeta` VALUES (128,76,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (129,77,'_wp_attached_file','2020/04/pasta-1.png');
INSERT INTO `wp_postmeta` VALUES (130,77,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:480;s:6:\"height\";i:712;s:4:\"file\";s:19:\"2020/04/pasta-1.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"pasta-1-202x300.png\";s:5:\"width\";i:202;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"pasta-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"pasta-1-480x660.png\";s:5:\"width\";i:480;s:6:\"height\";i:660;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"pasta-1-480x660.png\";s:5:\"width\";i:480;s:6:\"height\";i:660;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (131,76,'_thumbnail_id','77');
INSERT INTO `wp_postmeta` VALUES (132,78,'_edit_lock','1587430109:1');
INSERT INTO `wp_postmeta` VALUES (133,78,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (134,79,'_wp_attached_file','2020/04/pasta-2.jpg');
INSERT INTO `wp_postmeta` VALUES (135,79,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:700;s:6:\"height\";i:982;s:4:\"file\";s:19:\"2020/04/pasta-2.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"pasta-2-214x300.jpg\";s:5:\"width\";i:214;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"pasta-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"pasta-2-700x660.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"pasta-2-700x660.jpg\";s:5:\"width\";i:700;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (136,78,'_thumbnail_id','79');
INSERT INTO `wp_postmeta` VALUES (137,80,'_edit_lock','1587431411:1');
INSERT INTO `wp_postmeta` VALUES (138,80,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (139,82,'_wp_attached_file','2020/04/pasta-3.jpg');
INSERT INTO `wp_postmeta` VALUES (140,82,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:680;s:6:\"height\";i:680;s:4:\"file\";s:19:\"2020/04/pasta-3.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"pasta-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"pasta-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"pasta-3-680x660.jpg\";s:5:\"width\";i:680;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"pasta-3-680x660.jpg\";s:5:\"width\";i:680;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (141,80,'_thumbnail_id','82');
INSERT INTO `wp_postmeta` VALUES (142,83,'_edit_lock','1587431651:1');
INSERT INTO `wp_postmeta` VALUES (143,83,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (144,84,'_wp_attached_file','2020/04/d-1.jpg');
INSERT INTO `wp_postmeta` VALUES (145,84,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:2048;s:6:\"height\";i:1608;s:4:\"file\";s:15:\"2020/04/d-1.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"d-1-300x236.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:236;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"d-1-1024x804.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:804;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"d-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"d-1-768x603.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:603;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:17:\"d-1-1536x1206.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:1206;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"d-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"d-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (146,83,'_thumbnail_id','84');
INSERT INTO `wp_postmeta` VALUES (147,85,'_edit_lock','1587431907:1');
INSERT INTO `wp_postmeta` VALUES (148,85,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (149,86,'_wp_attached_file','2020/04/d-2.jpg');
INSERT INTO `wp_postmeta` VALUES (150,86,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:770;s:6:\"height\";i:1027;s:4:\"file\";s:15:\"2020/04/d-2.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"d-2-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"d-2-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"d-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"d-2-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:15:\"d-2-770x660.jpg\";s:5:\"width\";i:770;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:15:\"d-2-770x660.jpg\";s:5:\"width\";i:770;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (151,85,'_thumbnail_id','86');
INSERT INTO `wp_postmeta` VALUES (152,87,'_edit_lock','1587432073:1');
INSERT INTO `wp_postmeta` VALUES (153,87,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (154,88,'_wp_attached_file','2020/04/d-3.jpg');
INSERT INTO `wp_postmeta` VALUES (155,88,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:275;s:6:\"height\";i:183;s:4:\"file\";s:15:\"2020/04/d-3.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"d-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (156,87,'_thumbnail_id','88');
INSERT INTO `wp_postmeta` VALUES (157,90,'_wp_attached_file','2020/04/j-1.jpg');
INSERT INTO `wp_postmeta` VALUES (158,90,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:740;s:6:\"height\";i:400;s:4:\"file\";s:15:\"2020/04/j-1.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"j-1-300x162.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:162;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (159,91,'_wp_attached_file','2020/04/j-2.jpg');
INSERT INTO `wp_postmeta` VALUES (160,91,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:512;s:6:\"height\";i:197;s:4:\"file\";s:15:\"2020/04/j-2.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"j-2-300x115.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:115;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (161,92,'_wp_attached_file','2020/04/j-3.jpg');
INSERT INTO `wp_postmeta` VALUES (162,92,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:283;s:6:\"height\";i:178;s:4:\"file\";s:15:\"2020/04/j-3.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (163,93,'_wp_attached_file','2020/04/j-4.jpg');
INSERT INTO `wp_postmeta` VALUES (164,93,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:512;s:6:\"height\";i:288;s:4:\"file\";s:15:\"2020/04/j-4.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"j-4-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-4-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (165,94,'_wp_attached_file','2020/04/j-5.jpg');
INSERT INTO `wp_postmeta` VALUES (166,94,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:720;s:6:\"height\";i:340;s:4:\"file\";s:15:\"2020/04/j-5.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"j-5-300x142.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:142;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (167,95,'_wp_attached_file','2020/04/j-6.jpg');
INSERT INTO `wp_postmeta` VALUES (168,95,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1300;s:6:\"height\";i:957;s:4:\"file\";s:15:\"2020/04/j-6.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"j-6-300x221.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"j-6-1024x754.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:754;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"j-6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"j-6-768x565.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:565;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"j-6-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"j-6-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (169,96,'_edit_lock','1587440515:1');
INSERT INTO `wp_postmeta` VALUES (170,96,'_edit_last','1');
INSERT INTO `wp_postmeta` VALUES (171,97,'_wp_attached_file','2020/04/drink-3.jpg');
INSERT INTO `wp_postmeta` VALUES (172,97,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2020/04/drink-3.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-3-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"drink-3-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-3-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-3-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (173,98,'_wp_attached_file','2020/04/drink-5.jpg');
INSERT INTO `wp_postmeta` VALUES (174,98,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:19:\"2020/04/drink-5.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-5-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-5-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"drink-5-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-5-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-5-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (175,99,'_wp_attached_file','2020/04/drink-6.jpg');
INSERT INTO `wp_postmeta` VALUES (176,99,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:984;s:4:\"file\";s:19:\"2020/04/drink-6.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-6-244x300.jpg\";s:5:\"width\";i:244;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-6-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"drink-6-768x945.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:945;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-6-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-6-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (177,100,'_wp_attached_file','2020/04/drink-7.jpg');
INSERT INTO `wp_postmeta` VALUES (178,100,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:826;s:4:\"file\";s:19:\"2020/04/drink-7.jpg\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-7-291x300.jpg\";s:5:\"width\";i:291;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-7-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"drink-7-768x793.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:793;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-7-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-7-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (179,101,'_wp_attached_file','2020/04/drink-8.jpg');
INSERT INTO `wp_postmeta` VALUES (180,101,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:800;s:6:\"height\";i:1067;s:4:\"file\";s:19:\"2020/04/drink-8.jpg\";s:5:\"sizes\";a:6:{s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"drink-8-225x300.jpg\";s:5:\"width\";i:225;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"drink-8-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"drink-8-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"drink-8-768x1024.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:19:\"drink-8-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:19:\"drink-8-800x660.jpg\";s:5:\"width\";i:800;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (181,102,'_wp_attached_file','2020/04/c-1.jpg');
INSERT INTO `wp_postmeta` VALUES (182,102,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1566;s:6:\"height\";i:881;s:4:\"file\";s:15:\"2020/04/c-1.jpg\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"c-1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"c-1-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"c-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"c-1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:16:\"c-1-1536x864.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"menusLandscape\";a:4:{s:4:\"file\";s:16:\"c-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"menusPortrait\";a:4:{s:4:\"file\";s:16:\"c-1-1000x660.jpg\";s:5:\"width\";i:1000;s:6:\"height\";i:660;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}');
INSERT INTO `wp_postmeta` VALUES (183,96,'_thumbnail_id','100');
/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_posts`
--

DROP TABLE IF EXISTS `wp_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_posts`
--

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;
INSERT INTO `wp_posts` VALUES (1,1,'2020-03-27 20:18:46','2020-03-27 20:18:46','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','trash','open','open','','hello-world__trashed','','','2020-04-20 06:44:37','2020-04-20 06:44:37','',0,'http://squisito-1.local/?p=1',0,'post','',1);
INSERT INTO `wp_posts` VALUES (2,1,'2020-03-27 20:18:46','2020-03-27 20:18:46','<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://squisito-1.local/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->','Sample Page','','publish','closed','open','','sample-page','','','2020-03-27 20:18:46','2020-03-27 20:18:46','',0,'http://squisito-1.local/?page_id=2',0,'page','',0);
INSERT INTO `wp_posts` VALUES (3,1,'2020-03-27 20:18:46','2020-03-27 20:18:46','<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://squisito-1.local.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->','Privacy Policy','','draft','closed','open','','privacy-policy','','','2020-03-27 20:18:46','2020-03-27 20:18:46','',0,'http://squisito-1.local/?page_id=3',0,'page','',0);
INSERT INTO `wp_posts` VALUES (5,1,'2020-03-27 23:38:26','2020-03-27 23:38:26','','About','','publish','closed','closed','','about','','','2020-04-11 22:43:01','2020-04-11 22:43:01','',0,'http://squisito-1.local/?page_id=5',0,'page','',0);
INSERT INTO `wp_posts` VALUES (6,1,'2020-03-27 23:38:26','2020-03-27 23:38:26','<!-- wp:paragraph -->\n<p>We love pizza</p>\n<!-- /wp:paragraph -->','About','','inherit','closed','closed','','5-revision-v1','','','2020-03-27 23:38:26','2020-03-27 23:38:26','',5,'http://squisito-1.local/5-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (8,1,'2020-04-10 23:32:20','2020-04-10 23:32:20','<!-- wp:paragraph -->\n<p>Squitio hosts many menus.</p>\n<!-- /wp:paragraph -->','Menu','','trash','closed','closed','','menu__trashed','','','2020-04-20 07:41:15','2020-04-20 07:41:15','',0,'http://squisito-1.local/?page_id=8',0,'page','',0);
INSERT INTO `wp_posts` VALUES (9,1,'2020-04-10 23:32:20','2020-04-10 23:32:20','<!-- wp:paragraph -->\n<p>Squitio hosts many menus.</p>\n<!-- /wp:paragraph -->','Menu','','inherit','closed','closed','','8-revision-v1','','','2020-04-10 23:32:20','2020-04-10 23:32:20','',8,'http://squisito-1.local/8-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (10,1,'2020-04-10 23:32:49','2020-04-10 23:32:49','<!-- wp:paragraph -->\n<p>New deals every week!</p>\n<!-- /wp:paragraph -->','Deals','','publish','closed','closed','','deals','','','2020-04-10 23:32:49','2020-04-10 23:32:49','',0,'http://squisito-1.local/?page_id=10',0,'page','',0);
INSERT INTO `wp_posts` VALUES (11,1,'2020-04-10 23:32:49','2020-04-10 23:32:49','<!-- wp:paragraph -->\n<p>New deals every week!</p>\n<!-- /wp:paragraph -->','Deals','','inherit','closed','closed','','10-revision-v1','','','2020-04-10 23:32:49','2020-04-10 23:32:49','',10,'http://squisito-1.local/10-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (12,1,'2020-04-11 11:34:59','2020-04-11 11:34:59','','Home','','publish','closed','closed','','home','','','2020-04-11 11:34:59','2020-04-11 11:34:59','',0,'http://squisito-1.local/?page_id=12',0,'page','',0);
INSERT INTO `wp_posts` VALUES (13,1,'2020-04-11 11:34:59','2020-04-11 11:34:59','','Home','','inherit','closed','closed','','12-revision-v1','','','2020-04-11 11:34:59','2020-04-11 11:34:59','',12,'http://squisito-1.local/12-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (14,1,'2020-04-11 11:35:14','2020-04-11 11:35:14','','Blog','','publish','closed','closed','','blog','','','2020-04-11 11:35:14','2020-04-11 11:35:14','',0,'http://squisito-1.local/?page_id=14',0,'page','',0);
INSERT INTO `wp_posts` VALUES (15,1,'2020-04-11 11:35:14','2020-04-11 11:35:14','','Blog','','inherit','closed','closed','','14-revision-v1','','','2020-04-11 11:35:14','2020-04-11 11:35:14','',14,'http://squisito-1.local/14-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (16,1,'2020-04-11 11:47:44','2020-04-11 11:47:44','<!-- wp:paragraph -->\n<p>Stay tuned for what we have in store. </p>\n<!-- /wp:paragraph -->','Opening Soon!','','publish','open','open','','opening','','','2020-04-20 06:52:04','2020-04-20 06:52:04','',0,'http://squisito-1.local/?p=16',0,'post','',0);
INSERT INTO `wp_posts` VALUES (17,1,'2020-04-11 11:47:44','2020-04-11 11:47:44','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Opening!','','inherit','closed','closed','','16-revision-v1','','','2020-04-11 11:47:44','2020-04-11 11:47:44','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (18,1,'2020-04-11 22:43:01','2020-04-11 22:43:01','','About','','inherit','closed','closed','','5-revision-v1','','','2020-04-11 22:43:01','2020-04-11 22:43:01','',5,'http://squisito-1.local/5-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (19,1,'2020-04-11 22:59:40','2020-04-11 22:59:40','','Services','','publish','closed','closed','','services','','','2020-04-11 22:59:40','2020-04-11 22:59:40','',0,'http://squisito-1.local/?page_id=19',0,'page','',0);
INSERT INTO `wp_posts` VALUES (20,1,'2020-04-11 22:59:40','2020-04-11 22:59:40','','Services','','inherit','closed','closed','','19-revision-v1','','','2020-04-11 22:59:40','2020-04-11 22:59:40','',19,'http://squisito-1.local/19-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (21,1,'2020-04-11 23:01:42','2020-04-11 23:01:42','','Contact','','publish','closed','closed','','contact','','','2020-04-11 23:01:42','2020-04-11 23:01:42','',0,'http://squisito-1.local/?page_id=21',0,'page','',0);
INSERT INTO `wp_posts` VALUES (22,1,'2020-04-11 23:01:42','2020-04-11 23:01:42','','Contact','','inherit','closed','closed','','21-revision-v1','','','2020-04-11 23:01:42','2020-04-11 23:01:42','',21,'http://squisito-1.local/21-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (23,1,'2020-04-19 22:52:19','0000-00-00 00:00:00','','Auto Draft','','auto-draft','open','open','','','','','2020-04-19 22:52:19','0000-00-00 00:00:00','',0,'http://squisito-1.local/?p=23',0,'post','',0);
INSERT INTO `wp_posts` VALUES (24,1,'2020-04-19 22:54:54','2020-04-19 22:54:54','<img class=\"alignnone size-full wp-image-25\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pizza-deal1.jpg\" alt=\"\" width=\"285\" height=\"177\" />\r\n\r\nFor your celebration, you can now buy any 2 Pizza and 2L Coca Cola for only $12.99.\r\n\r\nHappy Easter to All!!!','Easter Special','','publish','closed','closed','','happy-easter-deal','','','2020-04-20 13:59:03','2020-04-20 13:59:03','',0,'http://squisito-1.local/?post_type=deal&#038;p=24',0,'deal','',0);
INSERT INTO `wp_posts` VALUES (25,1,'2020-04-19 22:54:00','2020-04-19 22:54:00','','pizza-deal1','','inherit','open','closed','','pizza-deal1','','','2020-04-19 22:54:00','2020-04-19 22:54:00','',24,'http://squisito-1.local/wp-content/uploads/2020/04/pizza-deal1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (27,1,'2020-04-20 06:30:18','2020-04-20 06:30:18','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:image -->\n<figure class=\"wp-block-image\"><img alt=\"\"/></figure>\n<!-- /wp:image -->','Opening!','','inherit','closed','closed','','16-revision-v1','','','2020-04-20 06:30:18','2020-04-20 06:30:18','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (28,1,'2020-04-20 06:31:31','2020-04-20 06:31:31','','Opening','','inherit','open','closed','','opening-2','','','2020-04-20 06:31:31','2020-04-20 06:31:31','',16,'http://squisito-1.local/wp-content/uploads/2020/04/Opening.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (29,1,'2020-04-20 06:32:05','2020-04-20 06:32:05','<!-- wp:image {\"id\":28,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://squisito-1.local/wp-content/uploads/2020/04/Opening.jpg\" alt=\"\" class=\"wp-image-28\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Opening!','','inherit','closed','closed','','16-revision-v1','','','2020-04-20 06:32:05','2020-04-20 06:32:05','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (30,1,'2020-04-20 06:37:18','2020-04-20 06:37:18','','opening','','inherit','open','closed','','opening-3','','','2020-04-20 06:37:18','2020-04-20 06:37:18','',16,'http://squisito-1.local/wp-content/uploads/2020/04/opening-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (31,1,'2020-04-20 06:37:24','2020-04-20 06:37:24','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Opening!','','inherit','closed','closed','','16-revision-v1','','','2020-04-20 06:37:24','2020-04-20 06:37:24','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (32,1,'2020-04-20 06:44:13','2020-04-20 06:44:13','<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->','Hello world!','','inherit','closed','closed','','1-revision-v1','','','2020-04-20 06:44:13','2020-04-20 06:44:13','',1,'http://squisito-1.local/1-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (33,1,'2020-04-20 06:46:19','2020-04-20 06:46:19','<!-- wp:paragraph -->\n<p>Earlier this week marked our first-ever opening of Squisto and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Open!','','publish','open','open','','open','','','2020-04-20 06:48:59','2020-04-20 06:48:59','',0,'http://squisito-1.local/?p=33',0,'post','',0);
INSERT INTO `wp_posts` VALUES (34,1,'2020-04-20 06:46:19','2020-04-20 06:46:19','','Open','','inherit','closed','closed','','33-revision-v1','','','2020-04-20 06:46:19','2020-04-20 06:46:19','',33,'http://squisito-1.local/33-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (35,1,'2020-04-20 06:46:40','2020-04-20 06:46:40','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Opening Soon!','','inherit','closed','closed','','16-revision-v1','','','2020-04-20 06:46:40','2020-04-20 06:46:40','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (36,1,'2020-04-20 06:46:52','2020-04-20 06:46:52','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squistio and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Open!','','inherit','closed','closed','','33-revision-v1','','','2020-04-20 06:46:52','2020-04-20 06:46:52','',33,'http://squisito-1.local/33-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (37,1,'2020-04-20 06:46:57','2020-04-20 06:46:57','<!-- wp:paragraph -->\n<p>Earlier this week marked our first ever opening of Squisto and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Open!','','inherit','closed','closed','','33-revision-v1','','','2020-04-20 06:46:57','2020-04-20 06:46:57','',33,'http://squisito-1.local/33-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (38,1,'2020-04-20 06:47:07','2020-04-20 06:47:07','<!-- wp:paragraph -->\n<p>Earlier this week marked our first-ever opening of Squisto and we are completely blown away by the positive response you have all shown!</p>\n<!-- /wp:paragraph -->','Open!','','inherit','closed','closed','','33-revision-v1','','','2020-04-20 06:47:07','2020-04-20 06:47:07','',33,'http://squisito-1.local/33-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (39,1,'2020-04-20 06:48:45','2020-04-20 06:48:45','','open','','inherit','open','closed','','open-2','','','2020-04-20 06:48:45','2020-04-20 06:48:45','',33,'http://squisito-1.local/wp-content/uploads/2020/04/open.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (40,1,'2020-04-20 06:50:14','2020-04-20 06:50:14','','Opening Soon!','','inherit','closed','closed','','16-autosave-v1','','','2020-04-20 06:50:14','2020-04-20 06:50:14','',16,'http://squisito-1.local/16-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (41,1,'2020-04-20 06:52:04','2020-04-20 06:52:04','<!-- wp:paragraph -->\n<p>Stay tuned for what we have in store. </p>\n<!-- /wp:paragraph -->','Opening Soon!','','inherit','closed','closed','','16-revision-v1','','','2020-04-20 06:52:04','2020-04-20 06:52:04','',16,'http://squisito-1.local/16-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (42,1,'2020-04-20 07:29:22','2020-04-20 07:29:22','It made simply with Prosciutto Crudo and mushrooms.','Pizza Veronese','','trash','closed','closed','','pizza-veronese__trashed','','','2020-04-20 07:53:49','2020-04-20 07:53:49','',0,'http://squisito-1.local/?post_type=menus&#038;p=42',0,'menus','',0);
INSERT INTO `wp_posts` VALUES (43,1,'2020-04-20 07:29:01','2020-04-20 07:29:01','','p-1','','inherit','open','closed','','p-1','','','2020-04-20 07:29:01','2020-04-20 07:29:01','',42,'http://squisito-1.local/wp-content/uploads/2020/04/p-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (44,1,'2020-04-20 08:20:01','2020-04-20 08:20:01','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"deal\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','Deals Date','deals-date','publish','closed','closed','','group_5e9d5a9c7bfe5','','','2020-04-20 08:20:01','2020-04-20 08:20:01','',0,'http://squisito-1.local/?post_type=acf-field-group&#038;p=44',0,'acf-field-group','',0);
INSERT INTO `wp_posts` VALUES (45,1,'2020-04-20 08:20:01','2020-04-20 08:20:01','a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:6:\"F j, Y\";s:13:\"return_format\";s:6:\"F j, Y\";s:9:\"first_day\";i:1;}','Deals Date','deals_date','publish','closed','closed','','field_5e9d5abde65bc','','','2020-04-20 08:20:01','2020-04-20 08:20:01','',44,'http://squisito-1.local/?post_type=acf-field&p=45',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (46,1,'2020-04-20 08:20:01','2020-04-20 08:20:01','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','','','publish','closed','closed','','field_5e9d5b0be65bd','','','2020-04-20 08:20:01','2020-04-20 08:20:01','',44,'http://squisito-1.local/?post_type=acf-field&p=46',1,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (47,1,'2020-04-20 08:22:04','2020-04-20 08:22:04','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"deal\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','Related Deals','related-deals','publish','closed','closed','','group_5e9d5b4f105f8','','','2020-04-20 14:39:53','2020-04-20 14:39:53','',0,'http://squisito-1.local/?post_type=acf-field-group&#038;p=47',0,'acf-field-group','',0);
INSERT INTO `wp_posts` VALUES (48,1,'2020-04-20 08:22:04','2020-04-20 08:22:04','a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";s:0:\"\";s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}','Related Deal(s)','related_deals','publish','closed','closed','','field_5e9d5b5a4282e','','','2020-04-20 08:22:04','2020-04-20 08:22:04','',47,'http://squisito-1.local/?post_type=acf-field&p=48',0,'acf-field','',0);
INSERT INTO `wp_posts` VALUES (49,1,'2020-04-20 08:51:32','2020-04-20 08:51:32','','Menu','','trash','closed','closed','','menu__trashed-2','','','2020-04-20 08:53:02','2020-04-20 08:53:02','',0,'http://squisito-1.local/?page_id=49',0,'page','',0);
INSERT INTO `wp_posts` VALUES (50,1,'2020-04-20 08:51:32','2020-04-20 08:51:32','','Menu','','inherit','closed','closed','','49-revision-v1','','','2020-04-20 08:51:32','2020-04-20 08:51:32','',49,'http://squisito-1.local/49-revision-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (51,1,'2020-04-20 09:00:09','2020-04-20 09:00:09','It is made simply with Prosciutto Crudo and mushrooms.','Pizza Veronese','','publish','closed','closed','','pizza-veronese','','','2020-04-20 10:43:45','2020-04-20 10:43:45','',0,'http://squisito-1.local/?post_type=menus&#038;p=51',0,'menus','',0);
INSERT INTO `wp_posts` VALUES (52,1,'2020-04-20 12:57:56','2020-04-20 12:57:56','<img class=\"alignnone size-medium wp-image-43\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-1-295x300.jpg\" alt=\"\" width=\"295\" height=\"300\" />\r\n\r\nThis one is made simply with Prosciutto Crudo and mushrooms.','Pizza Veronese','','publish','closed','closed','','pizza-veronese','','','2020-04-20 22:57:02','2020-04-20 22:57:02','',0,'http://squisito-1.local/?post_type=menu&#038;p=52',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (53,1,'2020-04-20 13:36:23','0000-00-00 00:00:00','','Auto Draft','','auto-draft','closed','closed','','','','','2020-04-20 13:36:23','0000-00-00 00:00:00','',0,'http://squisito-1.local/?post_type=menu&p=53',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (54,1,'2020-04-20 13:57:13','2020-04-20 13:57:13','<img class=\"alignnone size-full wp-image-25\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pizza-deal1.jpg\" alt=\"\" width=\"285\" height=\"177\" />\n\nFor your celebration, you can now buy 2 Pizza and 2L Coca Cola for only','Easter Special','','inherit','closed','closed','','24-autosave-v1','','','2020-04-20 13:57:13','2020-04-20 13:57:13','',24,'http://squisito-1.local/24-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (55,1,'2020-04-20 13:59:42','0000-00-00 00:00:00','','Auto Draft','','auto-draft','closed','closed','','','','','2020-04-20 13:59:42','0000-00-00 00:00:00','',0,'http://squisito-1.local/?post_type=reviews&p=55',0,'reviews','',0);
INSERT INTO `wp_posts` VALUES (57,2,'2020-04-20 15:28:09','2020-04-20 15:28:09','I had a great time at this restaurant and the food was amazing. It was such an experience!!!','Food','','publish','closed','closed','','food','','','2020-04-20 15:28:09','2020-04-20 15:28:09','',0,'http://squisito-1.local/?post_type=review&#038;p=57',0,'review','',0);
INSERT INTO `wp_posts` VALUES (58,1,'2020-04-20 21:25:04','2020-04-20 21:25:04','<img class=\"alignnone size-full wp-image-59\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-2.jpg\" alt=\"\" width=\"235\" height=\"214\" />\r\n\r\n<em>Topped with ricotta and mozzarella cheeses with fresh garlic and oil</em>','Bianca','','publish','closed','closed','','bianca','','','2020-04-20 22:56:31','2020-04-20 22:56:31','',0,'http://squisito-1.local/?post_type=menu&#038;p=58',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (59,1,'2020-04-20 21:24:45','2020-04-20 21:24:45','','p-2','','inherit','open','closed','','p-2','','','2020-04-20 21:24:45','2020-04-20 21:24:45','',58,'http://squisito-1.local/wp-content/uploads/2020/04/p-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (60,1,'2020-04-20 21:26:09','2020-04-20 21:26:09','&nbsp;\n\n<em>Topped with ricotta and mozzarella cheeses with fresh garlic and oil</em>','Bianca','','inherit','closed','closed','','58-autosave-v1','','','2020-04-20 21:26:09','2020-04-20 21:26:09','',58,'http://squisito-1.local/58-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (61,1,'2020-04-20 21:37:38','2020-04-20 21:37:38','<img class=\"size-medium wp-image-62 alignleft\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-3-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<p style=\"text-align: left;\"><em>Includes pepperoni, sausage, meatballs, bacon, and ham</em></p>','Meat Lover’s Special','','publish','closed','closed','','meat-lovers-special','','','2020-04-20 22:55:53','2020-04-20 22:55:53','',0,'http://squisito-1.local/?post_type=menu&#038;p=61',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (62,1,'2020-04-20 21:37:03','2020-04-20 21:37:03','','p-3','','inherit','open','closed','','p-3','','','2020-04-20 21:37:03','2020-04-20 21:37:03','',61,'http://squisito-1.local/wp-content/uploads/2020/04/p-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (63,1,'2020-04-20 21:46:22','2020-04-20 21:46:22','<img class=\"size-medium wp-image-64 alignleft\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pizza-7-1-300x286.jpg\" alt=\"\" width=\"300\" height=\"286\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<p style=\"text-align: left;\"><em>Include Fresh mushrooms, fresh bell peppers, fresh onions, fresh sliced marinated tomatoes, and cheese</em></p>','“The Don”','','publish','closed','closed','','the-don','','','2020-04-20 22:55:26','2020-04-20 22:55:26','',0,'http://squisito-1.local/?post_type=menu&#038;p=63',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (64,1,'2020-04-20 21:46:04','2020-04-20 21:46:04','','pizza-7','','inherit','open','closed','','pizza-7','','','2020-04-20 21:46:04','2020-04-20 21:46:04','',63,'http://squisito-1.local/wp-content/uploads/2020/04/pizza-7-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (65,1,'2020-04-20 22:01:37','2020-04-20 22:01:37','<img class=\"alignnone size-medium wp-image-66\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-4-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n\r\nIncludes artichoke, olives, mushrooms &amp; soppressata','Capricciosa','','publish','closed','closed','','capricciosa','','','2020-04-20 22:54:41','2020-04-20 22:54:41','',0,'http://squisito-1.local/?post_type=menu&#038;p=65',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (66,1,'2020-04-20 22:01:23','2020-04-20 22:01:23','','p-4','','inherit','open','closed','','p-4','','','2020-04-20 22:01:23','2020-04-20 22:01:23','',65,'http://squisito-1.local/wp-content/uploads/2020/04/p-4.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (67,1,'2020-04-20 22:12:02','2020-04-20 22:12:02','&nbsp;\r\n\r\n<img class=\"alignnone size-medium wp-image-68\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pizza-13-2-300x200.jpg\" alt=\"\" width=\"300\" height=\"200\" />\r\n\r\nIncludes fresh spinach, fresh tomato, fresh mushroom, mozzarella, and Parmigiano','Veggie','','publish','closed','closed','','veggie','','','2020-04-20 22:54:25','2020-04-20 22:54:25','',0,'http://squisito-1.local/?post_type=menu&#038;p=67',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (68,1,'2020-04-20 22:11:16','2020-04-20 22:11:16','','pizza-13','','inherit','open','closed','','pizza-13','','','2020-04-20 22:11:16','2020-04-20 22:11:16','',67,'http://squisito-1.local/wp-content/uploads/2020/04/pizza-13-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (69,1,'2020-04-20 22:34:46','2020-04-20 22:34:46','A tasty gluten-free cheese pizza\r\n\r\n<img class=\"alignnone size-medium wp-image-70 alignleft\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-5-300x225.png\" alt=\"\" width=\"300\" height=\"225\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<p style=\"text-align: left;\"></p>','Gluten Free Cheese Pizza','','publish','closed','closed','','gluten-free-cheese-pizza','','','2020-04-21 03:40:01','2020-04-21 03:40:01','',0,'http://squisito-1.local/?post_type=menu&#038;p=69',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (70,1,'2020-04-20 22:32:19','2020-04-20 22:32:19','','p-5','','inherit','open','closed','','p-5','','','2020-04-20 22:32:19','2020-04-20 22:32:19','',69,'http://squisito-1.local/wp-content/uploads/2020/04/p-5.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (71,1,'2020-04-20 22:40:16','2020-04-20 22:40:16','<img class=\"alignnone size-medium wp-image-72 alignleft\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-6-300x180.jpg\" alt=\"\" width=\"300\" height=\"180\" />\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n\r\n&nbsp;\r\n<p style=\"text-align: left;\">Including  garlic chicken, tomatoes, Asiago Cheese, breakfast bacon</p>','Laz Special Pizza','','publish','closed','closed','','laz-special-pizza','','','2020-04-20 22:53:43','2020-04-20 22:53:43','',0,'http://squisito-1.local/?post_type=menu&#038;p=71',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (72,1,'2020-04-20 22:39:13','2020-04-20 22:39:13','','p-6','','inherit','open','closed','','p-6','','','2020-04-20 22:39:13','2020-04-20 22:39:13','',71,'http://squisito-1.local/wp-content/uploads/2020/04/p-6.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (73,1,'2020-04-20 22:53:42','2020-04-20 22:53:42','<img class=\"alignnone size-medium wp-image-72 alignleft\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/p-6-300x180.jpg\" alt=\"\" width=\"300\" height=\"180\" />\n\n&nbsp;\n\n&nbsp;\n\n&nbsp;\n\n&nbsp;\n\n&nbsp;\n<p style=\"text-align: left;\">Including  garlic chicken, tomatoes, Asiago Cheese, breakfast bacon</p>','Laz Special Pizza','','inherit','closed','closed','','71-autosave-v1','','','2020-04-20 22:53:42','2020-04-20 22:53:42','',71,'http://squisito-1.local/71-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (74,1,'2020-04-20 23:02:19','2020-04-20 23:02:19','Includes Pineapple, Orange, Passionfruit, Fruit Punch and lemonade.\r\n\r\n&nbsp;\r\n\r\n[gallery columns=\"2\" ids=\"90,92,93,94,91,95\" orderby=\"rand\"]\r\n\r\n&nbsp;','Juices','','publish','closed','closed','','lemonade-juice','','','2020-04-21 03:45:05','2020-04-21 03:45:05','',0,'http://squisito-1.local/?post_type=menu&#038;p=74',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (75,1,'2020-04-20 23:02:03','2020-04-20 23:02:03','','drink-1','','inherit','open','closed','','drink-1','','','2020-04-20 23:02:03','2020-04-20 23:02:03','',74,'http://squisito-1.local/wp-content/uploads/2020/04/drink-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (76,1,'2020-04-21 00:43:05','2020-04-21 00:43:05','<img class=\"alignnone size-medium wp-image-77\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pasta-1-202x300.png\" alt=\"\" width=\"202\" height=\"300\" />\r\n\r\nIncluding Mozzarella, Fontina, Ricotta, fresh Parmesan, and Asiago','5-Cheese Marinara Pasta','','publish','closed','closed','','5-cheese-marinara-pasta','','','2020-04-21 00:43:05','2020-04-21 00:43:05','',0,'http://squisito-1.local/?post_type=menu&#038;p=76',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (77,1,'2020-04-21 00:41:15','2020-04-21 00:41:15','','pasta-1','','inherit','open','closed','','pasta-1','','','2020-04-21 00:41:15','2020-04-21 00:41:15','',76,'http://squisito-1.local/wp-content/uploads/2020/04/pasta-1.png',0,'attachment','image/png',0);
INSERT INTO `wp_posts` VALUES (78,1,'2020-04-21 00:50:43','2020-04-21 00:50:43','<img class=\"alignnone size-medium wp-image-79\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pasta-2-214x300.jpg\" alt=\"\" width=\"214\" height=\"300\" />\r\n\r\nIncludes parmesan cheese, baby spinach, mushrooms, and grilled chicken','Parmesan Creamy Pasta','','publish','closed','closed','','parmesan-creamy-pasta','','','2020-04-21 00:50:43','2020-04-21 00:50:43','',0,'http://squisito-1.local/?post_type=menu&#038;p=78',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (79,1,'2020-04-21 00:50:05','2020-04-21 00:50:05','','pasta-2','','inherit','open','closed','','pasta-2','','','2020-04-21 00:50:05','2020-04-21 00:50:05','',78,'http://squisito-1.local/wp-content/uploads/2020/04/pasta-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (80,1,'2020-04-21 01:12:27','2020-04-21 01:12:27','<img class=\"alignnone size-medium wp-image-82\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/pasta-3-300x300.jpg\" alt=\"\" width=\"300\" height=\"300\" />\r\n\r\nIncluding tomatoes, spinach, bacon strips, and parmesan cheese','Creamy Garlic Pasta','','publish','closed','closed','','creamy-garlic-pasta','','','2020-04-21 01:12:33','2020-04-21 01:12:33','',0,'http://squisito-1.local/?post_type=menu&#038;p=80',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (81,1,'2020-04-21 01:11:20','0000-00-00 00:00:00','','Auto Draft','','auto-draft','closed','closed','','','','','2020-04-21 01:11:20','0000-00-00 00:00:00','',0,'http://squisito-1.local/?post_type=menu&p=81',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (82,1,'2020-04-21 01:12:14','2020-04-21 01:12:14','','pasta-3','','inherit','open','closed','','pasta-3','','','2020-04-21 01:12:14','2020-04-21 01:12:14','',80,'http://squisito-1.local/wp-content/uploads/2020/04/pasta-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (83,1,'2020-04-21 01:16:27','2020-04-21 01:16:27','<img class=\"alignnone size-medium wp-image-84\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/d-1-300x236.jpg\" alt=\"\" width=\"300\" height=\"236\" />\r\n\r\nTiramisu is a coffee-flavoured Italian dessert. It is made of ladyfingers dipped in coffee, layered with a whipped mixture of eggs, sugar, and mascarpone cheese, flavoured with cocoa.','Tiramisu','','publish','closed','closed','','tiramisu','','','2020-04-21 01:16:27','2020-04-21 01:16:27','',0,'http://squisito-1.local/?post_type=menu&#038;p=83',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (84,1,'2020-04-21 01:15:32','2020-04-21 01:15:32','','d-1','','inherit','open','closed','','d-1','','','2020-04-21 01:15:32','2020-04-21 01:15:32','',83,'http://squisito-1.local/wp-content/uploads/2020/04/d-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (85,1,'2020-04-21 01:19:42','2020-04-21 01:19:42','<img class=\"alignnone size-medium wp-image-86\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/d-2-225x300.jpg\" alt=\"\" width=\"225\" height=\"300\" />\r\n\r\nTartufo is an Italian ice cream dessert originating from Pizzo, Calabria. It is usually composed of two or more flavors of ice cream, often with either fruit syrup or frozen fruit — typically raspberry, strawberry, or cherry — in the center.','Tartufo','','publish','closed','closed','','tartufo','','','2020-04-21 01:19:42','2020-04-21 01:19:42','',0,'http://squisito-1.local/?post_type=menu&#038;p=85',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (86,1,'2020-04-21 01:19:26','2020-04-21 01:19:26','','d-2','','inherit','open','closed','','d-2','','','2020-04-21 01:19:26','2020-04-21 01:19:26','',85,'http://squisito-1.local/wp-content/uploads/2020/04/d-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (87,1,'2020-04-21 01:22:49','2020-04-21 01:22:49','<img class=\"alignnone size-full wp-image-88\" src=\"http://squisito-1.local/wp-content/uploads/2020/04/d-3.jpg\" alt=\"\" width=\"275\" height=\"183\" />\r\n\r\nZuppa Inglese is an Italian dessert layering custard and sponge cake, perhaps derived from a trifle.','Zuppa Inglese','','publish','closed','closed','','zuppa-inglese','','','2020-04-21 01:22:49','2020-04-21 01:22:49','',0,'http://squisito-1.local/?post_type=menu&#038;p=87',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (88,1,'2020-04-21 01:22:29','2020-04-21 01:22:29','','d-3','','inherit','open','closed','','d-3','','','2020-04-21 01:22:29','2020-04-21 01:22:29','',87,'http://squisito-1.local/wp-content/uploads/2020/04/d-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (89,1,'2020-04-21 02:26:01','2020-04-21 02:26:01','[gallery columns=\"2\" ids=\"90,92,93,94,91,95\" orderby=\"rand\"]\n\nIncludes Pineapple, Orange, Passionfruit, Fruit Punch, lemonade,','Juices','','inherit','closed','closed','','74-autosave-v1','','','2020-04-21 02:26:01','2020-04-21 02:26:01','',74,'http://squisito-1.local/74-autosave-v1/',0,'revision','',0);
INSERT INTO `wp_posts` VALUES (90,1,'2020-04-21 01:31:25','2020-04-21 01:31:25','','j-1','','inherit','open','closed','','j-1','','','2020-04-21 01:31:25','2020-04-21 01:31:25','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (91,1,'2020-04-21 01:31:25','2020-04-21 01:31:25','','j-2','','inherit','open','closed','','j-2','','','2020-04-21 01:31:25','2020-04-21 01:31:25','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-2.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (92,1,'2020-04-21 01:31:25','2020-04-21 01:31:25','','j-3','','inherit','open','closed','','j-3','','','2020-04-21 01:31:25','2020-04-21 01:31:25','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (93,1,'2020-04-21 01:31:26','2020-04-21 01:31:26','','j-4','','inherit','open','closed','','j-4','','','2020-04-21 01:31:26','2020-04-21 01:31:26','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-4.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (94,1,'2020-04-21 01:31:26','2020-04-21 01:31:26','','j-5','','inherit','open','closed','','j-5','','','2020-04-21 01:31:26','2020-04-21 01:31:26','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-5.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (95,1,'2020-04-21 02:25:52','2020-04-21 02:25:52','','j-6','','inherit','open','closed','','j-6','','','2020-04-21 02:25:52','2020-04-21 02:25:52','',74,'http://squisito-1.local/wp-content/uploads/2020/04/j-6.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (96,1,'2020-04-21 02:34:43','2020-04-21 02:34:43','Includes Non-alcohol and Alcoholic Beverages\r\n\r\n&nbsp;\r\n\r\n[gallery ids=\"97,98,99,100,101,102\"]','Cocktails','','publish','closed','closed','','cocktails','','','2020-04-21 03:43:40','2020-04-21 03:43:40','',0,'http://squisito-1.local/?post_type=menu&#038;p=96',0,'menu','',0);
INSERT INTO `wp_posts` VALUES (97,1,'2020-04-21 02:26:53','2020-04-21 02:26:53','','drink-3','','inherit','open','closed','','drink-3','','','2020-04-21 02:26:53','2020-04-21 02:26:53','',96,'http://squisito-1.local/wp-content/uploads/2020/04/drink-3.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (98,1,'2020-04-21 02:26:54','2020-04-21 02:26:54','','drink-5','','inherit','open','closed','','drink-5','','','2020-04-21 02:26:54','2020-04-21 02:26:54','',96,'http://squisito-1.local/wp-content/uploads/2020/04/drink-5.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (99,1,'2020-04-21 02:26:54','2020-04-21 02:26:54','','drink-6','','inherit','open','closed','','drink-6','','','2020-04-21 02:26:54','2020-04-21 02:26:54','',96,'http://squisito-1.local/wp-content/uploads/2020/04/drink-6.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (100,1,'2020-04-21 02:26:55','2020-04-21 02:26:55','','drink-7','','inherit','open','closed','','drink-7','','','2020-04-21 02:26:55','2020-04-21 02:26:55','',96,'http://squisito-1.local/wp-content/uploads/2020/04/drink-7.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (101,1,'2020-04-21 02:27:16','2020-04-21 02:27:16','','drink-8','','inherit','open','closed','','drink-8','','','2020-04-21 02:27:16','2020-04-21 02:27:16','',96,'http://squisito-1.local/wp-content/uploads/2020/04/drink-8.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (102,1,'2020-04-21 02:33:22','2020-04-21 02:33:22','','c-1','','inherit','open','closed','','c-1','','','2020-04-21 02:33:22','2020-04-21 02:33:22','',96,'http://squisito-1.local/wp-content/uploads/2020/04/c-1.jpg',0,'attachment','image/jpeg',0);
INSERT INTO `wp_posts` VALUES (103,1,'2020-04-21 02:36:04','2020-04-21 02:36:04','&nbsp;\n\nIn\n\n[gallery columns=\"2\" ids=\"97,98,99,100,101,102\"]','Cocktails','','inherit','closed','closed','','96-autosave-v1','','','2020-04-21 02:36:04','2020-04-21 02:36:04','',96,'http://squisito-1.local/96-autosave-v1/',0,'revision','',0);
/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_relationships`
--

DROP TABLE IF EXISTS `wp_term_relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_relationships`
--

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;
INSERT INTO `wp_term_relationships` VALUES (1,1,0);
INSERT INTO `wp_term_relationships` VALUES (16,2,0);
INSERT INTO `wp_term_relationships` VALUES (33,1,0);
INSERT INTO `wp_term_relationships` VALUES (52,3,0);
INSERT INTO `wp_term_relationships` VALUES (58,3,0);
INSERT INTO `wp_term_relationships` VALUES (61,3,0);
INSERT INTO `wp_term_relationships` VALUES (63,3,0);
INSERT INTO `wp_term_relationships` VALUES (65,3,0);
INSERT INTO `wp_term_relationships` VALUES (67,3,0);
INSERT INTO `wp_term_relationships` VALUES (69,3,0);
INSERT INTO `wp_term_relationships` VALUES (71,3,0);
INSERT INTO `wp_term_relationships` VALUES (74,4,0);
INSERT INTO `wp_term_relationships` VALUES (76,5,0);
INSERT INTO `wp_term_relationships` VALUES (78,5,0);
INSERT INTO `wp_term_relationships` VALUES (80,5,0);
INSERT INTO `wp_term_relationships` VALUES (83,6,0);
INSERT INTO `wp_term_relationships` VALUES (85,6,0);
INSERT INTO `wp_term_relationships` VALUES (87,6,0);
INSERT INTO `wp_term_relationships` VALUES (96,4,0);
/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_term_taxonomy`
--

DROP TABLE IF EXISTS `wp_term_taxonomy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_term_taxonomy`
--

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;
INSERT INTO `wp_term_taxonomy` VALUES (1,1,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (2,2,'category','',0,1);
INSERT INTO `wp_term_taxonomy` VALUES (3,3,'category','',0,8);
INSERT INTO `wp_term_taxonomy` VALUES (4,4,'category','',0,2);
INSERT INTO `wp_term_taxonomy` VALUES (5,5,'category','',0,3);
INSERT INTO `wp_term_taxonomy` VALUES (6,6,'category','',0,3);
/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_termmeta`
--

DROP TABLE IF EXISTS `wp_termmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_termmeta`
--

LOCK TABLES `wp_termmeta` WRITE;
/*!40000 ALTER TABLE `wp_termmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `wp_termmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_terms`
--

DROP TABLE IF EXISTS `wp_terms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_terms`
--

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;
INSERT INTO `wp_terms` VALUES (1,'Uncategorized','uncategorized',0);
INSERT INTO `wp_terms` VALUES (2,'Event','event',0);
INSERT INTO `wp_terms` VALUES (3,'Pizza','pizza',0);
INSERT INTO `wp_terms` VALUES (4,'Drinks','drinks',0);
INSERT INTO `wp_terms` VALUES (5,'Pasta','pasta',0);
INSERT INTO `wp_terms` VALUES (6,'Desserts','desserts',0);
/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_usermeta`
--

DROP TABLE IF EXISTS `wp_usermeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_usermeta`
--

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;
INSERT INTO `wp_usermeta` VALUES (1,1,'nickname','admin');
INSERT INTO `wp_usermeta` VALUES (2,1,'first_name','');
INSERT INTO `wp_usermeta` VALUES (3,1,'last_name','');
INSERT INTO `wp_usermeta` VALUES (4,1,'description','');
INSERT INTO `wp_usermeta` VALUES (5,1,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (6,1,'syntax_highlighting','true');
INSERT INTO `wp_usermeta` VALUES (7,1,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (8,1,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (9,1,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (10,1,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (11,1,'locale','');
INSERT INTO `wp_usermeta` VALUES (12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (13,1,'wp_user_level','10');
INSERT INTO `wp_usermeta` VALUES (14,1,'dismissed_wp_pointers','members_30');
INSERT INTO `wp_usermeta` VALUES (15,1,'show_welcome_panel','1');
INSERT INTO `wp_usermeta` VALUES (17,1,'wp_dashboard_quick_press_last_post_id','23');
INSERT INTO `wp_usermeta` VALUES (18,1,'wp_user-settings','libraryContent=browse');
INSERT INTO `wp_usermeta` VALUES (19,1,'wp_user-settings-time','1587435958');
INSERT INTO `wp_usermeta` VALUES (23,2,'nickname','Lucas');
INSERT INTO `wp_usermeta` VALUES (24,2,'first_name','');
INSERT INTO `wp_usermeta` VALUES (25,2,'last_name','');
INSERT INTO `wp_usermeta` VALUES (26,2,'description','');
INSERT INTO `wp_usermeta` VALUES (27,2,'rich_editing','true');
INSERT INTO `wp_usermeta` VALUES (28,2,'syntax_highlighting','true');
INSERT INTO `wp_usermeta` VALUES (29,2,'comment_shortcuts','false');
INSERT INTO `wp_usermeta` VALUES (30,2,'admin_color','fresh');
INSERT INTO `wp_usermeta` VALUES (31,2,'use_ssl','0');
INSERT INTO `wp_usermeta` VALUES (32,2,'show_admin_bar_front','true');
INSERT INTO `wp_usermeta` VALUES (33,2,'locale','');
INSERT INTO `wp_usermeta` VALUES (34,2,'wp_capabilities','a:1:{s:10:\"subscriber\";b:1;}');
INSERT INTO `wp_usermeta` VALUES (35,2,'wp_user_level','0');
INSERT INTO `wp_usermeta` VALUES (36,2,'default_password_nag','');
INSERT INTO `wp_usermeta` VALUES (40,1,'session_tokens','a:2:{s:64:\"fa6112581293b5bc136ec19853a671dffe7b8262f5ee8ddc3ae044ceaaa941fb\";a:4:{s:10:\"expiration\";i:1587590130;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36\";s:5:\"login\";i:1587417330;}s:64:\"19178585dda1a0f825a35d46d32a55394f496ec87bb48ee6597b5a160f9fcda8\";a:4:{s:10:\"expiration\";i:1587613134;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36\";s:5:\"login\";i:1587440334;}}');
INSERT INTO `wp_usermeta` VALUES (41,1,'closedpostboxes_menu','a:0:{}');
INSERT INTO `wp_usermeta` VALUES (42,1,'metaboxhidden_menu','a:1:{i:0;s:7:\"slugdiv\";}');
/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wp_users`
--

DROP TABLE IF EXISTS `wp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wp_users`
--

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;
INSERT INTO `wp_users` VALUES (1,'admin','$P$BiFPKvgaQsUMQphzGhpi1O2J10WZ6u0','admin','chanelle.mondesir@my.uwi.edu','','2020-03-27 20:18:46','',0,'admin');
INSERT INTO `wp_users` VALUES (2,'lucas','$P$Bc9aAA8HRldjAoyvz.JPg4GQ2gQbo..','lucas','cadmiumred37@networksfs.com','','2020-04-20 15:24:56','1587396297:$P$BLecttU4l7tKOS.G3EKVf4yZbZlwEZ0',0,'lucas');
/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-22  8:02:46
